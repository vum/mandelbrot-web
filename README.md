# Mandelbrot Web

Very simple implementation of the mandelbrot plot in JavaScript.

This was 4 years ago, to this day I don't know if my implementation
of different things like for example the coloring system is a good
approximation of what is widely used for this.

The black and white can also be done with gradients so it looks more
like a real mandelbrot but it looked cool without it.

![mandelbrot](pic.png)
![mandelbrot](pic2.png)